# Towards Automated Clinical Coding DL4H Spring 2023
## Citation
Finneas Catling, Georgios P Spithourakis, and Sebastian
Riedel. 2018. Towards automated clinical coding.
International journal of medical informatics, 120:50–
61.

## Dependencies
Hardware:
* Computer with AT LEAST 50GB of RAM (for peak running)
  * Training on preprocessed data only consumes ~10GB of RAM  
  
Python: 
* Python 3.10.11  
 
Libraries:  
* pandas  
* torch  
* torch.nn  
* numpy  
* torch.utils.data, Dataset  
* sklearn.metrics, precision_recall_fscore_support  
* re  
* gensim  
* sys  
* pickle  
* matplotlib.pyplot  

## Data Download
* To access physionet and the MIMIC3 dataset (NOTEEVENTS.csv, DIAGNOSES_ICD.csv), follow this process: https://eicu-crd.mit.edu/gettingstarted/access/
* To access the bin file for the w2v model, access here: http://evexdb.org/pmresources/vec-space-models/wikipedia-pubmed-and-PMC-w2v.bin
* To access the hierarchical data, visit here: https://drive.google.com/file/d/1EYlnup_ZDgnTbicNFnQZqDdrUwdsUvj-/view?usp=sharing

## Preprocessing Code
Preprocessing takes around 15 min to complete

Preprocessing the text (X) data
* utils.textExtract(w2v,"drive/My Drive/DL4H Final/NOTEEVENTS.csv")
  * Must provide word2vec model and path to file used. Notebook has exact code  

Preprocessing the label (Y) data
* utils.chapter1(df_diag_ICD.ICD9_CODE.dropna(),hierarchy)
  * Must provide datagrame with ICD9_CODE's and hierarchy (provided in pkl file in data download)  

Dataset, collate_fn
* utils.CustomDataset(df_dtm.X, df_dtm.Y)
* random_split(dataset, lengths)
  * Lengths is how many rows of training and validation you'd like in a two-element array, respectively
* utils.load_data(train_dataset, val_dataset, utils.collate_fn)
  * datasets comes from random_split   

Sample from dataset
* utils.sample_df(df_dtm,sample_size)
  * sample size between and include 0 and 1 is fraction of dataset, more than 1 is number of rows
    * we used the whole dataset for this project

Base Models
* utils.RNNGRU(embedding_weights,num_classes)
  * embedding weights comes from pre-processed w2v, num_classes
* utils.RNNLSTM(embedding_weights,num_classes)
  * Same as above

## Training Code
utils.train(model, train_loader, val_loader, n_epochs, optimizer, criterion)

## Evaluation Code
utils.eval_model(model, val_loader, print_val)
* print_val returns number of class 0 predicted and true (respectively)

## Pretrained model
GRU (46 min of training for a total of 5 epochs)
* GRU_Final.pkl  
  * https://drive.google.com/file/d/1wkYKYvRF79LofX3ek048s2CtMXZQx5tI/view?usp=sharing 
  
LSTM (43 min of training for a total of 5 epochs)
* LSTM_Final.pkl
  * https://drive.google.com/file/d/1--Oi-epeNofDua5KN8hNVl5nfKhblloM/view?usp=sharing

GRU, 4096 rows (4 min of training for a total of 5 epochs)
* GRU_4096.pkl  
  * https://drive.google.com/file/d/1-7CVCGQ62JPiYffz92J5Ed2C-_yJ4r6W/view?usp=sharing

LSTM, 4096 rows (4 min of training for a total of 5 epochs)
* LSTM_4096.pkl
  * https://drive.google.com/file/d/1-8GcTW5E09g5NzjxuWOTctaFWRrNfJbY/view?usp=sharing

## Table
| Model      | F1 Score | Precision | Recall |
| ---------- | -------- | --------- | ------ |
| TACC GRU   | 0.689    | 0.696     | 0.696  |
| GRU (100%) | 0.906    | 0.907     | 0.905  |
| LSTM (100%)| 0.922    | 0.908     | 0.937  |
| GRU (4096) | 0.934    | 0.913     | 0.956  |
| LSTM (4096)| 0.749    | 0.911     | 0.636  |
