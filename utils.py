import torch
import torch.nn as nn
import pandas as pd
import numpy as np
from torch.utils.data import Dataset
from sklearn.metrics import precision_recall_fscore_support
import re

# Inspiration: https://www.analyticsvidhya.com/blog/2022/01/tutorial-on-rnn-lstm-gru-with-implementation/
# Inspiration: https://towardsdatascience.com/5-simple-ways-to-tokenize-text-in-python-92c6804edfc4
def textExtract(w2v,path):
    # Takes a word2vec embedding model and pre-processes the text to a tokenized format
    #   Input:  w2v: word2vec embedding from wikipedia and medical docs
    #   Output: hopi_text: the processed text for history of present illness discharge summaries
    #           df.HADM_ID: the HADM_ID series from the NOTEEVENTS dataset
    #           indices: the indices of the text that has history of present illness
    #           unique tokens: the number of unique tokens that have been created
    #           w2v: the word2vec embedding model that has been updated
    def tokenCountRemove(series, w2v):
        # Takes a series of tokens and a replaces them with a pregenerated or random vector,
        # based on whether the token is in the word2vec embedding provided or not (respectively)
        #   Input:  series: An array of tokenized words
        #           w2v: a word2vec embedding from wikipedia and medical docs
        #   Output: {1}: a series that has maximum 500 tokens per row and has each token replaced with a corresponding index
        #           {2}: the number of unique indices used in the translation of the tokens
        #           w2v: an updated word2vec embedding
        dicts = {}
        key_list = []
        for lists in series:
            for element in lists[:500]:
                stripped = element.strip()
                if stripped != "":
                    if stripped in dicts.keys():
                        dicts[stripped] += 1
                    else:
                        dicts[stripped] = 0
        for key in dicts.keys():
            if dicts[key] < 5 or not key.isalpha():
                key_list.append(key)
        for key in key_list:
            dicts.pop(key)
        count = 0
        new_words = []
        new_vectors = []
        vector_size = w2v.vector_size
        for key in dicts.keys():
            if w2v.has_index_for(key) == False:
                new_words.append(key)
                new_vectors.append(np.random.rand(vector_size))
        # ChatGPT: how do I add a new word to a pretrained embedding
        print(new_words)
        print(new_vectors)
        w2v.add_vectors(new_words, new_vectors)
        return series.apply(lambda x: tokenTruncate(x, dicts, w2v)), len(list(dicts.keys())), w2v

    def tokenTruncate(text, dicts, w2v):
        # Takes a row from a series and translates each token into an index
        #   Input:  text: the tokens to be truncated and replaced with an index
        #           dicts: the dictionary that has whether or not the token exists in the embedding
        #           w2v: a word2vec embedding
        #   Output: new_text: the truncated and translated-to-index tokens
        new_text = []
        for element in text:
            stripped = element.strip()[:500]
            if stripped != "":
                if stripped in dicts.keys():
                    if w2v.has_index_for(stripped):
                        new_text.append(w2v.get_index(stripped))
        return new_text
    df = pd.read_csv(path)
    df = df[df["CATEGORY"] == "Discharge summary"]
    df = df.loc[:, df.columns.intersection(['HADM_ID', 'TEXT'])]
    hopi_text = df.TEXT.str.extract(
        r".*History of Present Illness(.*)", flags=re.DOTALL, expand=False)
    hopi_text = hopi_text.dropna()
    hopi_text_split = hopi_text.str.split()
    hopi_text_count_remove, unique_tokens, w2v = tokenCountRemove(
        hopi_text_split, w2v)
    hopi_text_rejoin = hopi_text_count_remove.dropna()
    indices = hopi_text_rejoin.index
    return hopi_text_rejoin, df.HADM_ID, indices, unique_tokens, w2v


def chapter1(series, hierarchy):
    # Takes a series of ICD9 codes and converts them into chapter 1 categories by index
    #   Input:  series: An array ICD9 codes
    #           hierarchy: a dataframe with the mapping from ICD9 code to chapter 1
    #   Output: new_series: an array of indexed chapters
    #           count: the total number of unique chapters indexed
    chapter1_dict = {}
    count = 0
    new_series = series
    new_series = new_series.apply(lambda x: 0)
    for index, x in enumerate(series):
        chapter = list(hierarchy[hierarchy["code"] == x]["chapter"])[0]
        if chapter not in chapter1_dict:
            chapter1_dict[chapter] = count
            count += 1
        new_series[index] = chapter1_dict[chapter]
    return new_series, count


def sample_df(df_dtm, sample_size):
    # Takes dataframe and sample_size, determines whether to sample by fraction or n, and returns the sample
    #   Input:  df_dtm: the dataframe to be sampled
    #           sample_size: between 0 and 1 inclusive is used as fraction, otherwise take the full number of samples
    #   Output: {1}: the sampled dataframe
    if sample_size <= 1 and sample_size > 0:
        return df_dtm.sample(frac=sample_size, ignore_index=True, random_state=1234)
    elif sample_size > 1:
        return df_dtm.sample(n=sample_size, ignore_index=True, random_state=1234)
    return None


class CustomDataset(Dataset):
    # Dataset to be used with data loader
    def __init__(self, x, y):

        self.x = x
        self.y = y

    def __len__(self):

        return len(self.x)

    def __getitem__(self, index):

        return (self.x[index], self.y[index])


def collate_fn(data):
    # Creates masks and data for dataloader
    batch_size = 128
    X, Y = zip(*data)
    y = torch.tensor(Y, dtype=torch.long)
    x = torch.zeros((batch_size, 500), dtype=torch.long)
    masks = torch.zeros((batch_size, 500), dtype=torch.long)
    rev_x = torch.zeros((batch_size, 500), dtype=torch.long)
    rev_masks = torch.zeros((batch_size, 500), dtype=torch.long)
    for index_i in range(len(Y)):
        for index_j in range(500):
            if index_j < len(X[index_i]):
                x[index_i, index_j] = X[index_i][index_j]
                masks[index_i, index_j] = 1
                rev_masks[index_i, index_j] = 1
                rev_x[index_i, index_j] = len(X[index_i]) - 1 - index_j

    return x, y, masks, rev_x, rev_masks


def load_data(train_dataset, val_dataset, collate_fn):
    # Adds data to dataloaders
    batch_size = 128
    train_loader = torch.utils.data.DataLoader(
        train_dataset, collate_fn=collate_fn, batch_size=batch_size, shuffle=True, drop_last=True)
    val_loader = torch.utils.data.DataLoader(
        val_dataset, collate_fn=collate_fn, batch_size=batch_size, shuffle=False, drop_last=True)
    return train_loader, val_loader


def sum_embeddings_with_mask(x, masks):
    """
    Arguments:
        x: the embeddings of diagnosis sequence of shape (batch_size, # visits, # diagnosis codes, embedding_dim)
        masks: the padding masks of shape (batch_size, # visits, # diagnosis codes)

    Outputs:
        sum_embeddings: the sum of embeddings of shape (batch_size, # visits, embedding_dim)

    """
    x = x*masks.unsqueeze(-1)
    x = torch.sum(x, dim=-2)
    return x


def get_last_visit(hidden_states, masks):
    """
    Arguments:
        hidden_states: the hidden states of each visit of shape (batch_size, # visits, embedding_dim)
        masks: the padding masks of shape (batch_size, # visits, # diagnosis codes)

    Outputs:
        last_hidden_state: the hidden state for the last true visit of shape (batch_size, embedding_dim)
    """
    num_visits = torch.argmin(masks, 0)

    return hidden_states[:, num_visits-1]


class RNNGRU(nn.Module):
    def __init__(self, embedding_weights, num_classes):
        super().__init__()
        emb_dim = len(embedding_weights[0])
        batch_size = 128
        self.embedding = torch.nn.Embedding.from_pretrained(
            embedding_weights, freeze=True)
        self.rnn = nn.GRU(emb_dim, hidden_size=batch_size,
                          batch_first=True, bidirectional=True)
        self.rev_rnn = nn.GRU(emb_dim, hidden_size=batch_size,
                              batch_first=True, bidirectional=True)
        self.fc = nn.Linear(1000, num_classes)
        self.act = nn.Softmax(1)
        self.do = nn.Dropout(.3)

    def forward(self, x, masks, rev_x, rev_masks):
        """
        Arguments:
            x: the diagnosis sequence of shape (batch_size, # visits, # diagnosis codes)
            masks: the padding masks of shape (batch_size, # visits, # diagnosis codes)

        Outputs:
            probs: probabilities of shape (batch_size)
        """
        x = self.embedding(x)
        x = sum_embeddings_with_mask(x, masks)
        output, _ = self.rnn(x)
        true_h_n = get_last_visit(output, masks)
        rev_x = self.embedding(rev_x)
        rev_x = sum_embeddings_with_mask(rev_x, rev_masks)
        output, _ = self.rev_rnn(rev_x)
        true_h_n_rev = get_last_visit(output, masks)
        logits = self.fc(self.do(torch.cat([true_h_n, true_h_n_rev], 1)))
        probs = self.act(logits)
        return probs


class RNNLSTM(nn.Module):
    def __init__(self, embedding_weights, num_classes):
        super().__init__()
        emb_dim = len(embedding_weights[0])
        batch_size = 128
        self.embedding = torch.nn.Embedding.from_pretrained(
            embedding_weights, freeze=True)
        self.rnn = nn.LSTM(emb_dim, hidden_size=batch_size,
                           batch_first=True, bidirectional=True)
        self.rev_rnn = nn.LSTM(emb_dim, hidden_size=batch_size,
                               batch_first=True, bidirectional=True)
        self.fc = nn.Linear(1000, num_classes)
        self.act = nn.Softmax(1)
        self.do = nn.Dropout(.3)

    def forward(self, x, masks, rev_x, rev_masks):
        """
        Arguments:
            x: the diagnosis sequence of shape (batch_size, # visits, # diagnosis codes)
            masks: the padding masks of shape (batch_size, # visits, # diagnosis codes)

        Outputs:
            probs: probabilities of shape (batch_size)
        """
        x = self.embedding(x)
        x = sum_embeddings_with_mask(x, masks)
        output, _ = self.rnn(x)
        true_h_n = get_last_visit(output, masks)
        rev_x = self.embedding(rev_x)
        rev_x = sum_embeddings_with_mask(rev_x, rev_masks)
        output, _ = self.rev_rnn(rev_x)
        true_h_n_rev = get_last_visit(output, masks)
        logits = self.fc(self.do(torch.cat([true_h_n, true_h_n_rev], 1)))
        probs = self.act(logits)
        return probs


def eval_model(model, val_loader, print_val):
    """

    Arguments:
        model: the RNN model
        val_loader: validation dataloader
        print_val: whether to return pred and true for graphs

    Outputs:
        precision: overall precision score
        recall: overall recall score
        f1: overall f1 score
        list_pred: list of predictions that are class 0
        list_true: list of true labels that are class 0

    """

    model.eval()
    y_pred = torch.LongTensor()
    y_true = torch.LongTensor()
    model.eval()
    for x, y, masks, rev_x, rev_masks in val_loader:
        y_hat = model(x, masks, rev_x, rev_masks)
        y_hat_new = torch.argmax(y_hat, 1)
        y_pred = torch.cat((y_pred, y_hat_new.detach().to('cpu')), dim=0)
        y_true = torch.cat((y_true, y.detach().to('cpu')), dim=0)
    # Inspiration: https://stats.stackexchange.com/questions/51296/how-do-you-calculate-precision-and-recall-for-multiclass-classification-using-co
    p, r, f, _ = precision_recall_fscore_support(
        y_true, y_pred, average='weighted')
    if print_val:
        list_pred = []
        list_true = []
        for pred in y_pred:
            if pred == 0:
                list_pred.append(pred)
        for true in y_true:
            if true == 0:
                list_true.append(true)
        print("Pred: " + str(list_pred))
        print("True: " + str(list_true))
    if print_val:
        return p, r, f, list_pred, list_true
    else:
        return p, r, f


def train(model, train_loader, val_loader, n_epochs, optimizer, criterion):
    """
    Arguments:
        model: the RNN model
        train_loader: training dataloder
        val_loader: validation dataloader
        n_epochs: total number of epochs
        
    Outputs:
        loss_append: array of loss values for graphing
        p_append: array of precision values for graphing
        r_append: array of recall values for graphing
        f_append: array of F1 scores for graphing

    """
    loss_append = []
    p_append = []
    r_append = []
    f_append = []
    for epoch in range(n_epochs):
        model.train()
        train_loss = 0
        count = 1
        for x, y, masks, rev_x, rev_masks in train_loader:
            optimizer.zero_grad()
            count += 1
            y_hat = model(x, masks, rev_x, rev_masks)
            loss = criterion(y_hat, y)
            loss.backward()
            optimizer.step()
            train_loss += loss.item()
        train_loss = train_loss / len(train_loader)
        loss_append.append(train_loss)
        print('Epoch: {} \t Training Loss: {:.6f}'.format(epoch+1, train_loss))
        p, r, f = eval_model(model, val_loader,False)
        p_append.append(p)
        r_append.append(r)
        f_append.append(f)
        print(p)
        print(r)
        print(f)
        print('Epoch: {} \t Validation p: {:.2f}, r:{:.2f}, f: {:.2f}'
              .format(epoch+1, p, r, f))
    return loss_append, p_append, r_append, f_append
